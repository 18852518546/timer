package com.zhongzhou.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhongzhou.api.entity.Email;
import com.zhongzhou.api.service.impl.EmailServiceImpl;
import com.zhongzhou.common.base.Pager;
import com.zhongzhou.common.bean.ReturnEntity;
import com.zhongzhou.common.bean.ReturnEntityError;
import com.zhongzhou.common.bean.ReturnEntitySuccess;
import com.zhongzhou.common.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *  发送邮箱历史信息
 *
 *
 */
@RestController
@RequestMapping("/api/email")
@Slf4j
public class EmailController {
    @Resource
    private EmailServiceImpl emailService;

    /**
     * 分页查询列表
     *
     * @param current    当前页码
     * @param size       每页数量
     * @param email Email
     * @return ReturnEntity
     */
    @GetMapping("/page")
    public ReturnEntity selectPageList(Email email, Integer current, Integer size,
                                       HttpServletRequest request, HttpServletResponse response){
        try{
            Pager<Email> pager = new Pager<>(current, size);
            QueryWrapper<Email> wrapper=new QueryWrapper<>();
            List<Email> records= emailService.page(pager,wrapper).getRecords();
            int count= emailService.count(wrapper);
            return new ReturnEntitySuccess(Constants.MSG_FIND_SUCCESS,count,records);
        }catch(Exception e){
            e.printStackTrace();
            log.error("["+Constants.MSG_FIND_FAILED+"]:"+e.getMessage());
            return new ReturnEntityError(Constants.MSG_FIND_FAILED,null,null);
        }
    }

    /**
     * 查询所有列表
     * @param email Email
     * @return ReturnEntity
     */
    @GetMapping("/list")
    public ReturnEntity selectList(Email email,
        HttpServletRequest request,HttpServletResponse response){
        try{
            QueryWrapper<Email> wrapper=new QueryWrapper<>();
            List<Email> list= emailService.list(wrapper);
            return new ReturnEntitySuccess(Constants.MSG_FIND_SUCCESS,list.size(),list);
        }catch(Exception e){
            e.printStackTrace();
            log.error("["+Constants.MSG_FIND_FAILED+"]:"+e.getMessage());
            return new ReturnEntityError(Constants.MSG_FIND_FAILED,null,null);
        }
    }

    /**
     * 查询详情
     *
     * @param id 主键
     * @return ReturnEntity
     */
    @GetMapping("/detail/{id}")
    public ReturnEntity selectById(@PathVariable("id") Long id){
        try{
            Email email = emailService.getById(id);
            if(null!= email){
                return new ReturnEntitySuccess(Constants.MSG_FIND_SUCCESS, email);
            }else{
                return new ReturnEntitySuccess(Constants.MSG_FIND_NOT_FOUND);
            }
        }catch(Exception e){
            e.printStackTrace();
            log.error("[id:{} " + Constants.MSG_FIND_FAILED + "]:{}", id, e.getMessage());
            return new ReturnEntityError(Constants.MSG_FIND_FAILED);
        }
    }

    /**
     * 新增
     *
     * @param email Email
     * @param result BindingResult
     * @return ReturnEntity
     */
    @PostMapping("/add")
    public ReturnEntity save(@Validated @RequestBody Email email,BindingResult result,
            HttpServletRequest request,HttpServletResponse response){
        if(result.hasErrors()){
            FieldError fieldError=result.getFieldErrors().get(0);
            String errorMsg=fieldError.getDefaultMessage();
            if(Constants.MSG_ERROR_CANNOT_NULL.equals(errorMsg)){
                errorMsg=fieldError.getField()+fieldError.getDefaultMessage();
            }
            return new ReturnEntityError(errorMsg,null, email);
        }else{
            try {
                QueryWrapper<Email> wrapper=new QueryWrapper<>();
                if(emailService.count(wrapper)>0){
                    return new ReturnEntityError(Constants.MSG_FIND_EXISTED, email);
                }else{
                    if(emailService.save(email)){
                        return new ReturnEntitySuccess(Constants.MSG_INSERT_SUCCESS, email);
                    }else{
                        return new ReturnEntityError(Constants.MSG_INSERT_FAILED, email);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("[" + Constants.MSG_INSERT_FAILED + "]:{}", e.getMessage());
                return new ReturnEntityError(Constants.MSG_INSERT_FAILED, email);
            }
        }
     }

    /**
     * 修改
     *
     * @param id 主键
     * @param email Email
     * @param result BindingResult
     * @return ReturnEntity
     */
    @PutMapping("/edit/{id}")
    public ReturnEntity updateById(@PathVariable("id") Long id,@Validated @RequestBody Email email,BindingResult result,
            HttpServletRequest request,HttpServletResponse response){
        if(result.hasErrors()){
            return new ReturnEntityError(result.getFieldErrors().get(0).getDefaultMessage(), email);
        }else{
            try {
                if(null== emailService.getById(id)){
                    return new ReturnEntityError(Constants.MSG_FIND_NOT_FOUND, email);
                }else{
                    if(emailService.updateById(email)){
                        return new ReturnEntitySuccess(Constants.MSG_UPDATE_SUCCESS, email);
                    }else{
                        return new ReturnEntityError(Constants.MSG_UPDATE_FAILED, email);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("[id:{} " + Constants.MSG_UPDATE_FAILED + "]:{}", id, e.getMessage());
                return new ReturnEntityError(Constants.MSG_UPDATE_FAILED, email);
            }
        }
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return ReturnEntity
     */
    @DeleteMapping("/delete/{id}")
    public ReturnEntity deleteById(@PathVariable("id") Long id){
        try {
            if(null== emailService.getById(id)){
                return new ReturnEntityError(Constants.MSG_FIND_NOT_FOUND,id);
            }else{
                if(emailService.removeById(id)){
                    return new ReturnEntitySuccess(Constants.MSG_DELETE_SUCCESS,id);
                }else{
                    return new ReturnEntityError(Constants.MSG_DELETE_FAILED,id);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[id:{} " + Constants.MSG_DELETE_FAILED + "]:{}", id, e.getMessage());
            return new ReturnEntityError(Constants.MSG_DELETE_FAILED, id);
        }
    }

}
