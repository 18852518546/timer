package com.zhongzhou.api.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhongzhou.api.entity.Basics;
import com.zhongzhou.api.entity.Email;
import com.zhongzhou.api.entity.HttpTest;
import com.zhongzhou.api.entity.Source;
import com.zhongzhou.api.service.*;
import com.zhongzhou.common.utils.HttpClientResult;
import com.zhongzhou.common.utils.HttpClientUtil;
import com.zhongzhou.common.utils.HttpClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 定时器调用接口
 */

@Controller
@RequestMapping("/api/httpClient")
@Slf4j
public class HttpClientController {

    @Autowired
    private ITimeService timeService;

    @Autowired
    private IBasicsService basicsService;

    @Autowired
    private IMailService mailService;

    @Autowired
    private IEmailService emailService;

    @Autowired
    private ISourceService sourceService;

    /**
     * 配置文件中我的qq邮箱
     */
    @Value("${spring.mail.from}")
    private String from;

    /**
     * 启动定时器
     *
     * @return
     */
    @RequestMapping(value = "/openMoreTimer", method = RequestMethod.POST)
    @ResponseBody
    @Scheduled(cron = "${cron}")
    public String openMoreTimer() {
        try {
            //查出库里所有的接口
            List<String> errList = new ArrayList<>();
            List<HttpTest> httpTestList = SourceList();
            if (httpTestList != null && httpTestList.size() != 0) {
                for (HttpTest httpTest : httpTestList) {
                    Integer code = 0;
                    CloseableHttpResponse response = null;
                    Integer status = 0;
                    if (httpTest.getMethod().equals("get")) {
                        try {
                            //get请求
                            response = new HttpClientUtil().sendGetData(httpTest.getUrl());
                            code = response.getStatusLine().getStatusCode();
                        } catch (Exception e) {
                            e.printStackTrace();
                            code = 404;
                        }
                    } else if (httpTest.getMethod().equals("post")) {
                        try {
                            Map<String, String> map = httpTest.getMap();
                            if (map.isEmpty()) {
                                status = 1;
                                HttpClientResult result = HttpClientUtils.postMethod(httpTest.getUrl(), 0);
                                code = result.getResponseCode();
                            } else {
                                status = 2;
                                //post请求
                                response = new HttpClientUtil().sendPostDataByMap(httpTest.getUrl(), httpTest.getMap());
                                code = response.getStatusLine().getStatusCode();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            code = 404;
                        }
                    }
                    log.info(httpTest.getDescription() + "," + "结果码" + code);
                    //如果结果码不等于200，就发邮箱通知
                    if (code != 200) {
                        //等待一分钟，再请求一遍校验。
                        Thread.sleep(1000 * 60);
                        try {
                            switch (status) {
                                case 0:
                                    response = new HttpClientUtil().sendGetData(httpTest.getUrl());
                                    code = response.getStatusLine().getStatusCode();
                                    break;
                                case 1:
                                    HttpClientResult result = HttpClientUtils.postMethod(httpTest.getUrl(), 0);
                                    code = result.getResponseCode();
                                    break;
                                case 2:
                                    response = new HttpClientUtil().sendPostDataByMap(httpTest.getUrl(), httpTest.getMap());
                                    code = response.getStatusLine().getStatusCode();
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            code = 404;
                        }
                        //如果隔一分钟之后还是code报错，就发邮件。
                        if (code != 200) {
                            // 错误说明
                            StringBuffer sb = new StringBuffer();
                            sb.append(httpTest.getDescription()).append(",url:").append(httpTest.getUrl()).append("，method:")
                                    .append(httpTest.getMethod()).append(",请求失败,code:").append(code);
                            if (!httpTest.getMap().isEmpty()) {
                                //字符串拼接参数
                                sb.append(",参数:");
                                for (Map.Entry<String, String> entry : httpTest.getMap().entrySet()) {
                                    sb.append(entry.getKey()).append("=").append(entry.getValue()).append(",");
                                }
                            }
                            errList.add(sb.toString());
//                            Email email = new Email();
//                            //存数据库的姓名
//                            email.setName("");
//                            //正文发送人邮箱
//                            email.setFromEmail("542704865@qq.com");
//                            //正文内容(url，post方式请求失败，code为。。)
//                            email.setContent("" + httpTest.getUrl() + "," + httpTest.getMethod() + "方式请求失败,code=" + code);
//                            if (sendMail(email).equals("success")) {
//                                log.info("发送邮箱成功");
//                            } else {
//                                log.error("发送邮箱失败");
//                            }
                        }
                    }
//                    Time timer = new Time();
//                    timer.setCode(code);
//                    timer.setUrl(httpTest.getUrl());
//                    timer.setMethod(httpTest.getMethod());
//                    timer.setCreateTime(LocalDateTime.now());
//                    if (timeService.save(timer)) {
//                        log.info("请求接口数据添加成功");
//                    } else {
//                        log.info("请求接口数据添加失败");
//                    }
                }

                if (!errList.isEmpty()) {
                    // 发邮件
                    Email email = new Email();
                    //存数据库的姓名
                    email.setName("");
                    //正文发送人邮箱
                    email.setFromEmail(from);
                    StringBuffer sb = new StringBuffer();
                    for (int i = 0; i < errList.size(); i++) {
                        sb.append("  ");
                        if (i == errList.size() - 1) {
                            sb.append(errList.get(i));
                        } else {
                            sb.append(errList.get(i)).append(",").append("\r\n");
                        }
                    }
                    //正文内容
                    email.setContent(sb.toString().substring(0, sb.length()));
                    if (sendMail(email).equals("success")) {
                        log.info("发送邮箱成功");
                    } else {
                        log.error("发送邮箱失败");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("接口请求失败");
        }
        return null;
    }

    /**
     * 发送邮箱
     *
     * @param email
     * @return
     */
    private String sendMail(Email email) {
        try {
            email.setCreateTime(LocalDateTime.now());
            //获取排序后第一条数据
            List<Basics> basicsInfos = basicsService.getLastBasicsInfo();
            String result = "success";
            for (Basics basicsInfo : basicsInfos) {
                if (basicsInfo == null) {
                    return "failure";
                }
                //设置收件人邮箱为总经理邮箱
                email.setToEmail(basicsInfo.getManageremail());
//                email.setUserId(JwtUtil.verifyToken(request));
                StringBuffer sb = new StringBuffer();
                //发件人邮箱
                sb.append("发件人邮箱：").append(email.getFromEmail()).append("\r\n");
                if (StringUtils.isNotBlank(email.getCompany())) {
                    sb.append("单位：").append(email.getCompany()).append("\r\n");
                }
                //正文
                sb.append("正文：").append("\r\n")
                        .append(email.getContent());
                mailService.sendSimpleMail(basicsInfo.getManageremail(), "您有一封来自服务器报错的信件。", sb.toString());
                log.info("发送邮件to：【{}】", basicsInfo.getManageremail());
                //如果保存失败就返回失败。
                if (emailService.save(email)) {
                    result = "success";
                } else {
                    result = "failure";
                    break;
                }
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return "" + e.getMessage();
        }
    }

    /**
     * 查询库里接口信息
     *
     * @return
     */
    private List<HttpTest> SourceList() {
        QueryWrapper<Source> wrapper = new QueryWrapper<>();
        List<Source> list = sourceService.queryList(wrapper);
        List<HttpTest> httpTests = new ArrayList<>();
        if (list != null && list.size() != 0) {
            for (Source source : list) {
                HttpTest httpTest = new HttpTest();
                httpTest.setUrl(source.getUrl());
                httpTest.setMethod(source.getMethod());
                httpTest.setDescription(source.getDescription());
                if (StringUtils.isNotBlank(source.getParam1()) && StringUtils.isNotBlank((source.getParam2()))) {
                    Map<String, String> map = new HashMap<>();
                    String param1[] = source.getParam1().split("&");
                    String param2[] = source.getParam2().split("&");
                    for (int i = 0; i < param1.length; i++) {
                        map.put(param1[i], param2[i]);
                    }
                    httpTest.setMap(map);
                }
                httpTests.add(httpTest);
            }
        }
        return httpTests;
    }
}
