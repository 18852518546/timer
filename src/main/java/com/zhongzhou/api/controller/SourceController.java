package com.zhongzhou.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.zhongzhou.api.entity.Source;
import com.zhongzhou.api.service.impl.SourceServiceimpl;
import com.zhongzhou.common.base.Pager;
import com.zhongzhou.common.bean.ReturnEntity;
import com.zhongzhou.common.bean.ReturnEntityError;
import com.zhongzhou.common.bean.ReturnEntitySuccess;
import com.zhongzhou.common.utils.AESUtil;
import com.zhongzhou.common.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 接口库增删改查
 *
 * @author wtl
 * @since 2020-12-23
 */
@RestController
@RequestMapping("/api/source")
@Slf4j
public class SourceController {
    @Resource
    private SourceServiceimpl sourceService;

    /**
     * 分页查询列表
     *
     * @param current 当前页码
     * @param size    每页数量
     * @param source  Source
     * @return ReturnEntity
     */
    @GetMapping("/page")
    public ReturnEntity selectPageList(Source source, Integer current, Integer size,
                                       HttpServletRequest request, HttpServletResponse response) {
        try {
            Pager<Source> pager = new Pager<>(current, size);
            QueryWrapper<Source> wrapper = new QueryWrapper<>();
            List<Source> records = sourceService.queryPage(pager, wrapper);
            int count = sourceService.queryCount(wrapper);
            return new ReturnEntitySuccess(Constants.MSG_FIND_SUCCESS, count, records);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[" + Constants.MSG_FIND_FAILED + "]:" + e.getMessage());
            return new ReturnEntityError(Constants.MSG_FIND_FAILED, null, null);
        }
    }

    /**
     * 查询所有列表
     *
     * @param source Source
     * @return ReturnEntity
     */
    @GetMapping("/list")
    public ReturnEntity selectList(Source source,
                                   HttpServletRequest request, HttpServletResponse response) {
        try {
            QueryWrapper<Source> wrapper = new QueryWrapper<>();
            List<Source> list = sourceService.queryList(wrapper);
            return new ReturnEntitySuccess(Constants.MSG_FIND_SUCCESS, list.size(), list);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[" + Constants.MSG_FIND_FAILED + "]:" + e.getMessage());
            return new ReturnEntityError(Constants.MSG_FIND_FAILED, null, null);
        }
    }

    /**
     * 查询详情
     *
     * @param id 主键
     * @return ReturnEntity
     */
    @GetMapping("/detail/{id}")
    public ReturnEntity selectById(@PathVariable("id") Long id) {
        try {
            Source source = sourceService.queryDetail(id);
            if (null != source) {
                return new ReturnEntitySuccess(Constants.MSG_FIND_SUCCESS, source);
            } else {
                return new ReturnEntitySuccess(Constants.MSG_FIND_NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[id:{} " + Constants.MSG_FIND_FAILED + "]:{}", id, e.getMessage());
            return new ReturnEntityError(Constants.MSG_FIND_FAILED);
        }
    }

    /**
     * 新增
     *
     * @param source Source
     * @param result BindingResult
     * @return ReturnEntity
     */
    @PostMapping("/add")
    public ReturnEntity save(@Validated @RequestBody Source source, BindingResult result,
                             HttpServletRequest request, HttpServletResponse response) {
        if (result.hasErrors()) {
            FieldError fieldError = result.getFieldErrors().get(0);
            String errorMsg = fieldError.getDefaultMessage();
            if (Constants.MSG_ERROR_CANNOT_NULL.equals(errorMsg)) {
                errorMsg = fieldError.getField() + fieldError.getDefaultMessage();
            }
            return new ReturnEntityError(errorMsg, null, source);
        } else {
            try {
                QueryWrapper<Source> wrapper = new QueryWrapper<>();
                if (sourceService.count(wrapper) > 0) {
                    return new ReturnEntityError(Constants.MSG_FIND_EXISTED, source);
                } else {
                    //加密
                    AESUtil.encryptSource(source);
                    //添加
                    if (sourceService.save(source)) {
                        return new ReturnEntitySuccess(Constants.MSG_INSERT_SUCCESS, source);
                    } else {
                        return new ReturnEntityError(Constants.MSG_INSERT_FAILED, source);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("[" + Constants.MSG_INSERT_FAILED + "]:{}", e.getMessage());
                return new ReturnEntityError(Constants.MSG_INSERT_FAILED, source);
            }
        }
    }

    /**
     * 修改
     *
     * @param id     主键
     * @param source Source
     * @param result BindingResult
     * @return ReturnEntity
     */
    @PutMapping("/edit/{id}")
    public ReturnEntity updateById(@PathVariable("id") Long id, @Validated @RequestBody Source source, BindingResult result,
                                   HttpServletRequest request, HttpServletResponse response) {
        if (result.hasErrors()) {
            return new ReturnEntityError(result.getFieldErrors().get(0).getDefaultMessage(), source);
        } else {
            try {
                if (null == sourceService.getById(id)) {
                    return new ReturnEntityError(Constants.MSG_FIND_NOT_FOUND, source);
                } else {
                    source.setId(id);
                    //加密
                    AESUtil.encryptSource(source);
                    if (sourceService.updateById(source)) {
                        return new ReturnEntitySuccess(Constants.MSG_UPDATE_SUCCESS, source);
                    } else {
                        return new ReturnEntityError(Constants.MSG_UPDATE_FAILED, source);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("[id:{} " + Constants.MSG_UPDATE_FAILED + "]:{}", id, e.getMessage());
                return new ReturnEntityError(Constants.MSG_UPDATE_FAILED, source);
            }
        }
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return ReturnEntity
     */
    @DeleteMapping("/delete/{id}")
    public ReturnEntity deleteById(@PathVariable("id") Long id) {
        try {
            if (null == sourceService.getById(id)) {
                return new ReturnEntityError(Constants.MSG_FIND_NOT_FOUND, id);
            } else {
                if (sourceService.removeById(id)) {
                    return new ReturnEntitySuccess(Constants.MSG_DELETE_SUCCESS, id);
                } else {
                    return new ReturnEntityError(Constants.MSG_DELETE_FAILED, id);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[id:{} " + Constants.MSG_DELETE_FAILED + "]:{}", id, e.getMessage());
            return new ReturnEntityError(Constants.MSG_DELETE_FAILED, id);
        }
    }


    /**
     * 加密所有
     *
     * @return ReturnEntity
     */
    @GetMapping("/encryptList")
    public ReturnEntity encryptList() {
        try {
            QueryWrapper<Source> wrapper = new QueryWrapper<>();
            List<Source> list = sourceService.list(wrapper);
            if(CollectionUtils.isNotEmpty(list)){
                for (Source source: list) {
                    AESUtil.encryptSource(source);
                    sourceService.updateById(source);
                }
            }
            return new ReturnEntitySuccess(Constants.MSG_FIND_SUCCESS, list.size(), list);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[" + Constants.MSG_FIND_FAILED + "]:" + e.getMessage());
            return new ReturnEntityError(Constants.MSG_FIND_FAILED, null, null);
        }
    }
}
