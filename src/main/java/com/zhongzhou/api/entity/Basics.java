package com.zhongzhou.api.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhongzhou.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("basics")

@Table(name = "basics")
 public class Basics extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 基础
     */
    @TableId("id")
    private Integer id;

    @TableField("phone")
    private String phone;


    @TableField("fax")
    private Integer fax;

    @TableField("email")
    private String email;

    @TableField("manageremail")
    private String manageremail;

    /**
     * 邮编
     */
    @TableField("postcode")
    private String postcode;

    /**
     * 备案号
     */
    @TableField("recordscode")
    private String recordscode;

    @TableField("address")
    private String address;

    /**
     * 二维码
     */
    @TableField("Qrcode")
    private String Qrcode;


   @TableField("latlng")
   private String latlng;


   private LocalDateTime createtime;



}
