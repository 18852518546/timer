package com.zhongzhou.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhongzhou.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_email")
@ApiModel(value="Email对象", description="")
public class Email extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "姓名", required = true)
    @TableField("name")
    @NotBlank(message = "姓名不能为空")
    private String name;

    @ApiModelProperty(value = "发件人邮箱", required = true)
    @TableField("from_email")
    @NotBlank(message = "请输入您的邮箱")
    private String fromEmail;

    @ApiModelProperty(value = "收件人邮箱")
    @TableField("to_email")
    private String toEmail;

    @ApiModelProperty(value = "单位")
    @TableField("company")
    private String company;

    @ApiModelProperty(value = "内容", required = true)
    @TableField("content")
    @NotBlank(message = "请输入留言内容")
    private String content;

    @TableField("user_id")
    private Long userId;

    @TableField("create_time")
    private LocalDateTime createTime;

    @TableField("del")
    private Integer del;
}
