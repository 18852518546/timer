package com.zhongzhou.api.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Data
public class HttpTest {
    //域名
    private String url;
    //请求方式
    private String method;
    //服务描述
    private String description;
    //用来存放参数
    private Map<String,String> map=new HashMap<String, String>();
    //参数字符串
    private String param;
}
