package com.zhongzhou.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.zhongzhou.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("source")
@ApiModel(value="Source对象", description="")
public class Source extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId(value = "id")
    private Long id;

    @TableField("url")
    private String url;

    @ApiModelProperty(value = "请求方式")
    @TableField("method")
    private String method;

    @ApiModelProperty(value = "参数key")
    @TableField("param1")
    private String param1;

    @ApiModelProperty(value = "参数value")
    @TableField("param2")
    private String param2;

    @ApiModelProperty(value = "服务描述")
    @TableField("description")
    private String description;

    @TableField("del")
    private Integer del;


}
