package com.zhongzhou.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhongzhou.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("time")
public class Time extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    @TableField("url")
    private String url;

    @TableField("method")
    private String method;

    @TableField("code")
    private Integer code;

    @TableField("param")
    private String param;

    @TableField("create_time")
    private LocalDateTime createTime;

    @TableField("del")
    private Integer del;
}
