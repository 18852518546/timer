package com.zhongzhou.api.mapper;

import com.zhongzhou.api.entity.Basics;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wj
 * @since 2020-07-29
 */
public interface BasicsMapper extends Mapper<Basics> {

    List<Basics> selectList();

    int deleteByid(Integer id);

    List<Basics> selectLastBasicsInfo();
}
