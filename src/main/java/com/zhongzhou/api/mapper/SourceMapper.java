package com.zhongzhou.api.mapper;

import com.zhongzhou.api.entity.Source;
import com.zhongzhou.api.entity.Time;
import com.zhongzhou.common.base.BaseDao;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zyx
 * @since 2020-08-03
 */
public interface SourceMapper extends BaseDao<Source> {
}
