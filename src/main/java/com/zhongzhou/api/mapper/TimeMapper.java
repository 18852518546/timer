package com.zhongzhou.api.mapper;

import com.zhongzhou.api.entity.Email;
import com.zhongzhou.api.entity.Time;
import com.zhongzhou.common.base.BaseDao;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zyx
 * @since 2020-08-03
 */
public interface TimeMapper extends BaseDao<Time> {
    Boolean save(Time time);
}
