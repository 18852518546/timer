package com.zhongzhou.api.service;


import com.zhongzhou.api.entity.Basics;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wj
 * @since 2020-07-29
 */
public interface IBasicsService {

    int saveAdd(Basics basics);

    List<Basics> getLastBasicsInfo();
}
