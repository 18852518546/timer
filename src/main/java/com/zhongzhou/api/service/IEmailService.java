package com.zhongzhou.api.service;

import com.zhongzhou.api.entity.Email;
import com.zhongzhou.common.base.BaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zyx
 * @since 2020-08-05
 */
public interface IEmailService extends BaseService<Email> {

}
