package com.zhongzhou.api.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhongzhou.api.entity.Source;
import com.zhongzhou.common.base.BaseService;
import com.zhongzhou.common.base.Pager;

import java.util.List;

/**
 * <p>
 * 服务类--附件
 * </p>
 *
 * @author wj
 * @since 2020-06-28
 */
public interface ISourceService extends BaseService<Source> {

    /**
     * 分页查询
     *
     * @param pager   分页
     * @param wrapper 条件
     * @return List<Source>
     */
    List<Source> queryPage(Pager<Source> pager, QueryWrapper<Source> wrapper);

    /**
     * 查询总数
     *
     * @param wrapper 条件
     * @return 总数
     */
    Integer queryCount(QueryWrapper<Source> wrapper);

    /**
     * 查询所有
     *
     * @param wrapper 条件
     * @return List<Source>
     */
    List<Source> queryList(QueryWrapper<Source> wrapper);

    /**
     * 查询详情
     *
     * @param id 主键
     * @return Source
     */
    Source queryDetail(Long id);
}
