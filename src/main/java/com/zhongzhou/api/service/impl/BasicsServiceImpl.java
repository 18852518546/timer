package com.zhongzhou.api.service.impl;

import com.zhongzhou.api.entity.Basics;
import com.zhongzhou.api.mapper.BasicsMapper;
import com.zhongzhou.api.service.IBasicsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wj
 * @since 2020-07-29
 */
@Service
public class BasicsServiceImpl implements IBasicsService {
    @Autowired
    BasicsMapper basicsMapper;

    @Override
    public int saveAdd(Basics basics) {
        int insert = basicsMapper.insertSelective(basics);
        return insert;
    }

    @Override
    public List<Basics> getLastBasicsInfo() {
        return basicsMapper.selectLastBasicsInfo();
    }


}
