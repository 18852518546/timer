package com.zhongzhou.api.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhongzhou.api.entity.Source;
import com.zhongzhou.api.mapper.SourceMapper;
import com.zhongzhou.api.service.ISourceService;
import com.zhongzhou.common.base.Pager;
import com.zhongzhou.common.utils.AESUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SourceServiceimpl extends ServiceImpl<SourceMapper, Source> implements ISourceService {

    private static final long serialVersionUID = -6931503950796599669L;

    @Override
    public List<Source> queryPage(Pager<Source> pager, QueryWrapper<Source> wrapper) {
        List<Source> records = page(pager, wrapper).getRecords();
        //赋值
        setDataList(records);
        //return data
        return records;
    }

    @Override
    public Integer queryCount(QueryWrapper<Source> wrapper) {
        return count(wrapper);
    }

    @Override
    public List<Source> queryList(QueryWrapper<Source> wrapper) {
        List<Source> list = list(wrapper);
        //赋值
        setDataList(list);
        //return data
        return list;
    }

    @Override
    public Source queryDetail(Long id) {
        Source source = getById(id);
        //赋值
        setSingleData(source);
        //return data
        return source;
    }

    /**
     * 赋值
     */
    void setDataList(List<Source> list) {
        if (CollectionUtils.isNotEmpty(list)) {
            for (Source source : list) {
                setSingleData(source);
            }
        }
    }

    /**
     * 赋值
     */
    void setSingleData(Source source) {
        if (source != null) {
            //解密
            AESUtil.decryptSource(source);
        }
    }


}
