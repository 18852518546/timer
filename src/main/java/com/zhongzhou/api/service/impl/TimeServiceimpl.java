package com.zhongzhou.api.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhongzhou.api.entity.Time;
import com.zhongzhou.api.mapper.TimeMapper;
import com.zhongzhou.api.service.ITimeService;
import com.zhongzhou.common.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TimeServiceimpl extends ServiceImpl<TimeMapper,Time> implements ITimeService {
}
