package com.zhongzhou.common.bean;

/**
 * @description:
 * @author: zyx
 * @date:2020/5/26 16:47
 */
public enum ErrorCodeEnum {
    RELOGIN_ERROR("请登录", 10001, "登录失效或未登录"),
    LOGIN_ERROR("用户名或密码为空", 10002, "用户名或密码为空"),
    LOGIN_PASSWORD_ERROR("用户名或密码错误", 10003, "用户名或密码错误"),
    USER_ERROR("用户配置不正确，请联系管理员", 10004, "用户角色配置有问题"),
    REG_CODE_ERROR("验证码错误", 10010, "验证码错误"),
    REG_USER_EXIST("用户已存在", 100011, "用户已存在"),
    REG_CODE_COUNT_MAX("当前获取验证码次数已达到5次，请1小时后再试", 10012, "1小时内重复获取验证码达到5次"),
    REG_CODE_PUSH_WAITE("请勿重复提交", 10013, "重复发送验证码"),
    REG_CODE_EXP("验证码已过期，请重新获取", 10014, "根据手机号没有找到验证码"),
    REG_REG_PROCESS_ERROR("已审核的数据，无法重复操作", 10015, "已审核的数据，无法重复操作"),

    PARAM_IS_NULL("参数为空", 50001, "必填参数为空"),
    NOT_SUPPORT_SUFFIX_ERROR("不支持的文件格式，请使用\"gif\", \"jpeg\", \"jpg\", \"png\", \"doc\", \"docx\", \"pdf\", \"excel\"格式的图片", 50003, "无效的文件格式"),
    ID_IS_VAILD("无效的id", 50004, "根据id未找到数据"),
    BASIC_IS_NOT_FOUND("当前尚未配置收件箱，请通过其他方式联系", 50005, "没有基础信息数据，无法发送邮件"),
    UNSUPPORT_EDIT_SYSUSER_OPERATION("当前账号已通过审核，不支持修改供应商等信息", 50020, "账号状态不正确，只有审核被拒绝的账号才可以修改账号信息"),
    FILE_DOWNLOAD_ERROR("下载失败", 50010, "下载失败"),
    FILE_DOWNLOAD_NOT_FOUND_ERROR("id不正确，下载失败", 50011, "根据id未找到文件"),
    ;

    private String name;
    private Integer code;

    private String describe;

    ErrorCodeEnum(String name, Integer code, String describe) {
        this.name = name;
        this.code = code;
        this.describe = describe;
    }

    public String getName() {
        return name;
    }

    public Integer getCode() {
        return code;
    }

    public String getDescribe() {
        return describe;
    }
}
