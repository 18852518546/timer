package com.zhongzhou.common.bean;

import com.github.pagehelper.Page;

import java.util.Collection;
import java.util.List;

/**
 * @description:
 * @author: zyx
 * @date:2020/6/3 12:06
 */
public class PageInfo<T> {

    private int pageNum;
    private int pageSize;
    private int pages;
    private long total;
    private List<T> list;

    public PageInfo(List<T> list) {
        if (list != null) {
            this.list = list;
            if (list instanceof Page) {
                this.total = ((Page) list).getTotal();
            } else {
                this.total = (long) list.size();
            }
            if (list instanceof Page) {
                Page page = (Page) list;
                this.pageNum = page.getPageNum();
                this.pageSize = page.getPageSize();
                this.pages = page.getPages();
            } else if (list instanceof Collection) {
                this.pageNum = 1;
                this.pageSize = list.size();
                this.pages = this.pageSize > 0 ? 1 : 0;
            }
        }
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public List<T> getList() {
        return list;
    }
}
