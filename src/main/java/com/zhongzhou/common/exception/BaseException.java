package com.zhongzhou.common.exception;

import com.zhongzhou.common.bean.ErrorCodeEnum;

/**
 * @description:
 * @author: zyx
 * @date:2020/4/26 9:09
 */
public class BaseException extends RuntimeException {
    private static final long serialVersionUID = -2028919895039614744L;

    private int code;

    public BaseException(String message) {
        super(message);
    }

    public BaseException(ErrorCodeEnum errorEnum) {
        super(errorEnum.getName());
        this.code = errorEnum.getCode();
    }

    public BaseException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
