package com.zhongzhou.common.utils;

public class HttpClientResult {
    /** 请求结果*/
    private String result;
    /** 请求状态码*/
    private Integer responseCode;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }
}
