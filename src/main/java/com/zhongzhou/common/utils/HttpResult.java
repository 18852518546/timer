package com.zhongzhou.common.utils;

import java.io.Serializable;

public class HttpResult implements Serializable{

	private Integer code;
	private String msg;
	private Object data;
	private Long count;
	private Integer status;
	private  String time;
	private  String tmsg;


	public HttpResult(){}

//	public HttpResult(String code, String msg, Object data) {
//		this.code = code;
//		this.msg = msg;
//		this.data = data;
//	}
public String getTime() {
	return time;
}

	public void setTime(String time) {
		this.time = time;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTmsg() {
		return tmsg;
	}

	public void setTmsg(String tmsg) {
		this.tmsg = tmsg;
	}
}
