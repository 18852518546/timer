package com.zhongzhou.common.utils;

import lombok.Data;

import java.util.Map;

@Data
public class HttpTest {
    //域名
    private String url;
    //请求方式
    private String method;
    //用来存放参数
    private Map<String,String> map;
    //参数字符串
    private String param;
}
