package com.zhongzhou.common.utils;

import com.zhongzhou.api.entity.HttpTest;
import lombok.Data;

import java.util.List;

@Data
public class HttpTestList {
    List<HttpTest> httpTestList;
}
